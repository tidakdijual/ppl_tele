import telebot
from telebot import types

# Replace with your bot token
bot_token = "5577957447:AAEksOkRvyudkE941pDiozv7fggxyDvPLEo"

# Connect to bot
bot = telebot.TeleBot(bot_token)

# Define keyboard with options
keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
keyboard.add('Option 1', 'Option 2', 'Option 3')

# Define function to handle start message
@bot.message_handler(commands=['start'])
def handle_start(message):
    # Send welcome message and options
    bot.send_message(message.chat.id, "Welcome to the Telegram Bot!\n\nChoose an option:", reply_markup=keyboard)

# Define function to handle option selection
@bot.message_handler(func=lambda message: message.text in ['Option 1', 'Option 2', 'Option 3'])
def handle_option_selection(message):
    # Get selected option
    selected_option = message.text

    # Ask for user's name
    bot.send_message(message.chat.id, "Please enter your name:")
    bot.register_next_step_handler(message, handle_name_input)

# Define function to handle name input
def handle_name_input(message):
    # Get user's name
    user_name = message.text

    # Get user's Telegram username
    telegram_username = message.from_user.username

    # Save user information to database
    # save_user_info(user_name, telegram_username, selected_option)

    # Send confirmation message
    bot.send_message(message.chat.id, f"Thank you, {user_name}. Your information has been saved.")

# Function to save user information to MySQL database
def save_user_info(name, telegram_username, choice):
    # Replace with your MySQL database connection details
    db_connection = MySQLdb.connect(host="localhost", user="username", password="password", database="database_name")
    cursor = db_connection.cursor()

    # Insert user information into the table
    cursor.execute("INSERT INTO user_info (name, telegram_username, choice) VALUES (%s, %s, %s)", (name, telegram_username, choice))
    db_connection.commit()
    db_connection.close()

# Start the bot
bot.polling()