import openai
import os
from dotenv import load_dotenv
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler, CallbackContext
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
import logging
import mysql.connector
import datetime

# Load environment variables from .env file
load_dotenv()

# Enable logging
# Read the LOG_FILE_PATH value from the .env file
log_file_path = os.getenv('LOG_FILE_PATH')


MYSQL_HOST = os.getenv("MYSQL_HOST")
MYSQL_USER = os.getenv("MYSQL_USER")
MYSQL_PASSWORD = os.getenv("MYSQL_PASS")
MYSQL_DATABASE = os.getenv("MYSQL_DB")

SUPER_ADMIN = os.getenv("SUPER_ADMIN")

# Configure logging
logging.basicConfig(filename=log_file_path,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.ERROR)

# Read the API key from the environment variable
openai_api_key = os.getenv("OPENAI_API_KEY")

# Initialize the API key for OpenAI
openai.api_key = openai_api_key

# A list of authorized user IDs
# authorized_users = [YOUR_AUTHORIZED_USER_ID1, YOUR_AUTHORIZED_USER_ID2, ...]
# If using file, fill in authorized_users.txt, and save the user IDs, one per line.
authorized_users = []


def connect_to_database():
    try:
        return mysql.connector.connect(
            host=MYSQL_HOST,
            user=MYSQL_USER,
            password=MYSQL_PASSWORD,
            database=MYSQL_DATABASE
        )
    except mysql.connector.Error as err:
        print(f"Error connecting to database: {err}")
        return None

conn = connect_to_database()
cursor = conn.cursor()

poli = ['aa', 'Poli Umum', 'Poli Gigi', 'Poli KIA', 'Poli Lansia', 'Laboratorium']
# sts = ['Menunggu', 'Hadir', 'Tidak Hadir']

def chat(update, context):
    try:
        # user_id = update.message.from_user.id
        username = update.message.from_user.id

        texts = update.message.text

        # jika texts not in 1, 2, 3, 4, 5, 6, 7, 8, 9

        cursor.execute(f"SELECT * FROM pasien WHERE username = '{username}'")
        result = cursor.fetchone()
        
        # check table pasien where username = username
        
        # get tommorrow's date
        date = datetime.datetime.now() + datetime.timedelta(days=1)
        date = date.strftime('%d-%m-%Y')

        daten = datetime.datetime.now()
        daten = daten.strftime('%d-%m-%Y')

        pilihan = "Selamat Datang di Poli Kesehatan. Untuk pendafaran tanggal "+ str(date) +" silahkan pilih:\n1. Poli Umum\n2. Poli Gigi\n3. Poli KIA\n4. Poli Lansia\n5. Laboratorium\n\nKetik angka yang sesuai dengan menu yang diinginkan."

        print(result)

        if result is None:
            # insert into table pasien, username = username
            cursor.execute(f"INSERT INTO pasien (username) VALUES ('{username}')")
            # save data to database
            conn.commit()
            context.bot.send_message(
            chat_id=update.effective_chat.id, text=pilihan)
        else:
            if result[4] in ['0']:

                if texts in ['1', '2', '3', '4', '5']:
                    #update table pasien set riwayat = texts where username = username
                    cursor.execute(f"SELECT * FROM jadwal WHERE username = '{username}' AND jadwal = '{date}' AND poli = '{texts}'")
                    jadwal = cursor.fetchone()
                    if jadwal is not None:
                        #texts to int

                        context.bot.send_message(chat_id=update.effective_chat.id, text="anda sudah terdaftar dengan nomor urut " + str(jadwal[4]) + " di "+poli[int(texts)]+" pada tanggal " + jadwal[2] + ". Terima kasih")
                    else:
                        cursor.execute(f"UPDATE pasien SET riwayat = '{texts}' WHERE username = '{username}'")
                        conn.commit()
                        context.bot.send_message(chat_id=update.effective_chat.id, text="masukkan nama pasien")
                else:
                    if texts.isdigit():
                        balasan = "Input tidak valid. \n" + pilihan
                    else:
                        balasan = pilihan
                    context.bot.send_message(chat_id=update.effective_chat.id, text=balasan)

            
            elif result[4] in ['900']:
                # lastid = result[6]
                # update to jadwal where id = lastid set umur = texts
                if not texts.isdigit():
                    context.bot.send_message(chat_id=update.effective_chat.id, text="Umur harus berupa angka")
                    
                else:
                    cursor.execute(f"UPDATE jadwal SET umur = '{texts}' WHERE id = '{result[6]}'")

                    # set pasien where username set riwayat = 901
                    cursor.execute(f"UPDATE pasien SET riwayat = '901' WHERE username = '{username}'")  
                    conn.commit()

                    context.bot.send_message(chat_id=update.effective_chat.id, text="Apakah sebagai pasien BPJS / Umum? \n jawab 1 untuk pasien BPJS dan 2 untuk pasien Umum")
            
            elif result[4] in ['901']:
                # lastid = result[6]
                # update to jadwal where id = lastid set umur = texts
                if texts == '1':
                    cursor.execute(f"UPDATE pasien SET riwayat = '902' WHERE username = '{username}'")
                    conn.commit()
                    context.bot.send_message(chat_id=update.effective_chat.id, text="Masukkan Nomor BPJS?")
                else:
                    cursor.execute(f"UPDATE pasien SET riwayat = '905' WHERE username = '{username}'")
                    conn.commit()
                    context.bot.send_message(chat_id=update.effective_chat.id, text="Apa Keluhan Pasien?")

            elif result[4] in ['902']:
                if not texts.isdigit():
                    context.bot.send_message(chat_id=update.effective_chat.id, text="Nomor BPJS harus berupa angka")
                    
                else:
                    cursor.execute(f"UPDATE jadwal SET bpjs = '{texts}' WHERE id = '{result[6]}'")

                    # set pasien where username set riwayat = 901
                    cursor.execute(f"UPDATE pasien SET riwayat = '905' WHERE username = '{username}'")  
                    conn.commit()
                    context.bot.send_message(chat_id=update.effective_chat.id, text="Apa Keluhan Pasien?")
            
            elif result[4] in ['905']:
                cursor.execute(f"UPDATE jadwal SET keluhan = '{texts}' WHERE id = '{result[6]}'")
                cursor.execute(f"UPDATE pasien SET riwayat = '0', lastid = '0' WHERE username = '{username}'")
                conn.commit()
                # select from jadwal where id = result[6]
                cursor.execute(f"SELECT * FROM jadwal WHERE id = '{result[6]}'")
                jadwal = cursor.fetchone()

                context.bot.send_message(chat_id=update.effective_chat.id, text="anda telah berhasil terdaftar dengan nomor urut " + str(jadwal[4]) + " di "+poli[int(jadwal[3])]+" pada tanggal " + jadwal[2] + " atas nama " + jadwal[7] + ". Terima kasih")
                
                

            elif result[4] in ['1', '2', '3', '4', '5', '6', '7', '8']:
                
                #cek sudah daftar apa belum
                cursor.execute(f"SELECT * FROM jadwal WHERE username = '{username}' AND jadwal = '{date}' AND poli = '{result[4]}'")
                jadwal = cursor.fetchone()
                if jadwal is not None:
                    cursor.execute(f"UPDATE pasien SET riwayat = '0' WHERE username = '{username}'")
                    conn.commit()
                    context.bot.send_message(chat_id=update.effective_chat.id, text="anda sudah terdaftar dengan nomor urut " + str(jadwal[4]) + " di "+poli[int(result[4])]+" pada tanggal " + jadwal[2] + ". Terima kasih")
                else:
                    #select from jadwal where poli = result[4] and jadwal = date
                    cursor.execute(f"SELECT * FROM jadwal WHERE poli = '{result[4]}' AND jadwal = '{date}' order by urutan desc limit 1")
                    urut = cursor.fetchone()
                    nourut = 1
                    if urut is not None:
                        nourut = urut[4] + 1

                    #insert into jadwal, username = username, poli = result[4], nama = result[5]
                    cursor.execute(f"INSERT INTO jadwal (username, poli, nama, jadwal, urutan) VALUES ('{username}', '{result[4]}', '{texts}', '{date}', '{nourut}')")

                    # select from jadwal to get id
                    cursor.execute(f"SELECT * FROM jadwal WHERE username = '{username}' AND jadwal = '{date}' AND poli = '{result[4]}'")
                    jadwal = cursor.fetchone()

                    # update pasien where username set riwayat = 900 and lastid = jadwal[0]
                    cursor.execute(f"UPDATE pasien SET riwayat = '900', lastid = '{jadwal[0]}' WHERE username = '{username}'")
                    # cursor.execute(f"UPDATE pasien SET riwayat = '0' WHERE username = '{username}'")
                    conn.commit()

                    context.bot.send_message(chat_id=update.effective_chat.id, text="masukkan umur pasien")
                    

                    #context.bot.send_message(chat_id=update.effective_chat.id, text="anda telah berhasil terdaftar dengan nomor urut " + str(nourut) + " di "+poli[int(result[4])]+" pada tanggal " + date + ". Terima kasih")
            else:
                context.bot.send_message(chat_id=update.effective_chat.id, text=pilihan + " " + result[4] + " " + texts)


    except Exception as e:
        # Log the error
        response_error_message = "An error occurred while sending a message. Please try again later."
        context.bot.send_message(
            chat_id=update.effective_chat.id, text=response_error_message)
        logging.error("An error occurred while sending a message: %s", str(e))
        if os.getenv("ENV") == "dev":
            print("An error occurred while sending a message: %s", str(e))
        else:
            print("An error occurred while sending a message. Check the logs for more information.")

def info(update, context):
    date = datetime.datetime.now() + datetime.timedelta(days=1)
    date = date.strftime('%d-%m-%Y')
    username = update.message.from_user.id
    # select * from table jadwal where username = username and date = date
    cursor.execute(f"SELECT * FROM jadwal WHERE username = '{username}' AND jadwal = '{date}'")
    results = cursor.fetchall()

    if results is not None:
        text = "Berikut adalah informasi pendaftaran anda untuk tanggal " + str(date) + ":\n"
        count = 0
        for result in results:
            count += 1
            # result[4] to string
            text += poli[int(result[3])] + " No Urut " + str(result[4]) + " atas nama " + result[7] + " umur " + str(result[9]) + "\n"

        if count == 0:
            text = "Anda belum mendaftar untuk tanggal "+date
        
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="Anda belum mendaftar untuk tanggal "+date)    

def pasien(update, context):
    texts = update.message.text.replace('/pasien ', '')
    username = update.message.from_user.id
    date = datetime.datetime.now()
    date = date.strftime('%d-%m-%Y')
    text = 'Tidak ada informasi'
    # cek dokter where username = username
    cursor.execute(f"SELECT * FROM dokter WHERE username = '{username}'")
    result = cursor.fetchone()
    if result is None:
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return

    # if texts is numeric
    if texts.isdigit():
        cursor.execute(f"SELECT a.* FROM jadwal a left join dokter b on a.poli = b.poli WHERE b.username = '{username}' and a.jadwal = '{date}' and a.urutan = '{texts}'")
        result = cursor.fetchone()
        if result is not None:
            text = "Berikut adalah informasi pasien " + poli[int(result[3])] + " :\n"
            text += "Nama: " + result[7] + "\n"
            text += "No Urut: " + str(result[4]) + "\n"
            text += "Tanggal: " + result[2] + "\n"
            text += "Umur: " + str(result[9]) + "\n"
            text += "Keluhan: " + result[8] + "\n"
        else:
            text = "No Urut tidak ditemukan"
    else:
        cursor.execute(f"SELECT a.* FROM jadwal a left join dokter b on a.poli = b.poli WHERE b.username = '{username}' and a.jadwal = '{date}' order by a.urutan asc")
        result = cursor.fetchall()
        text = "Berikut adalah informasi pasien hari ini "+str(date)+":\n"
        for res in result:
            sts = 'Menunggu'
            if(res[5] == '1'):
                sts = 'Hadir'
            elif(res[5] == '2'):
                sts = 'Tidak Hadir'
            text += "No Urut: " + str(res[4]) + " - " + res[7] + " Umur " + str(res[9]) + " - "+sts+" \n"

    context.bot.send_message(chat_id=update.effective_chat.id, text=text)

def start(update, context):
    welcome_message = "Hi, I'm a chatbot for poli kesehatan. How can I help you today? or you can use /info to get your schedule."
    context.bot.send_message(chat_id=update.effective_chat.id,
                             text=welcome_message)

def antrian(update, context):
    # texts = update.message.text.replace('/antrian ', '')
    username = update.message.from_user.id
    date = datetime.datetime.now()
    date = date.strftime('%d-%m-%Y')
    text = 'Tidak ada informasi'
    # cek dokter where username = username
    cursor.execute(f"SELECT * FROM perawat WHERE username = '{username}'")
    result = cursor.fetchone()
    if result is None:
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return

    cursor.execute(f"SELECT a.* FROM jadwal a left join perawat b on a.poli = b.poli WHERE b.username = '{username}' and a.jadwal = '{date}' order by a.urutan asc")
    result = cursor.fetchall()
    text = "Berikut adalah antrian pasien hari ini "+str(date)+":\n"
    
    for res in result:
        sts = 'Menunggu'
        if(res[5] == '1'):
            sts = 'Hadir'
        elif(res[5] == '2'):
            sts = 'Tidak Hadir'
        text += "No Urut: " + str(res[4]) + " - " + res[7] + " Umur " + str(res[9]) + " / "+ str(res[10]) +" - "+sts+" \n"

    context.bot.send_message(chat_id=update.effective_chat.id, text=text)

def skip(update, context):
    texts = update.message.text.replace('/skip ', '')
    username = update.message.from_user.id
    date = datetime.datetime.now()
    date = date.strftime('%d-%m-%Y')
    text = 'Tidak ada informasi'
    # cek dokter where username = username
    cursor.execute(f"SELECT * FROM perawat WHERE username = '{username}'")
    result = cursor.fetchone()
    if result is None:
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return
    
    if texts.isdigit():
        # update jadwal set status = 2 where urutan = texts and jadwal = date and poli = result[2]
        cursor.execute(f"UPDATE jadwal SET status = '2' WHERE urutan = '{texts}' and jadwal = '{date}' and poli = '{result[2]}'")
        conn.commit()
        text = "Berhasil update status pasien no urut " + texts + " menjadi Tidak Hadir"
    else:
        text = 'Input harus berupa angka'

    context.bot.send_message(chat_id=update.effective_chat.id, text=text)

def ada(update, context):
    texts = update.message.text.replace('/ada ', '')
    username = update.message.from_user.id
    date = datetime.datetime.now()
    date = date.strftime('%d-%m-%Y')
    text = 'Tidak ada informasi'
    # cek dokter where username = username
    cursor.execute(f"SELECT * FROM perawat WHERE username = '{username}'")
    result = cursor.fetchone()
    if result is None:
        context.bot.send_message(chat_id=update.effective_chat.id, text=text)
        return
    
    if texts.isdigit():
        # update jadwal set status = 2 where urutan = texts and jadwal = date and poli = result[2]
        cursor.execute(f"UPDATE jadwal SET status = '1' WHERE urutan = '{texts}' and jadwal = '{date}' and poli = '{result[2]}'")
        conn.commit()
        text = "Berhasil update status pasien no urut " + texts + " menjadi Hadir"
    else:
        text = 'Input harus berupa angka'

    context.bot.send_message(chat_id=update.effective_chat.id, text=text)

def admin(update, context):
    # '/admin add perawat 1 456789'
    texts = update.message.text.replace('/admin ', '').strip().split(' ')
    username = update.message.from_user.id
    if int(username) != int(SUPER_ADMIN):
        context.bot.send_message(chat_id=update.effective_chat.id, text='Anda tidak diizinkan')
        return
    if texts[0] in ['add', 'remove'] and texts[1] in ['perawat', 'dokter'] and texts[2] in ['1', '2', '3', '4', '5'] and texts[3].isdigit():
        if texts[0] == 'add':
            # insert into table perawat/dokter where username = texts[2]
            cursor.execute(f"INSERT INTO {texts[1]} (poli, username) VALUES ('{texts[2]}', '{texts[3]}')")
            conn.commit()
            context.bot.send_message(chat_id=update.effective_chat.id, text='Berhasil menambahkan '+texts[1]+' ke '+poli[int(texts[2])]+' dengan user id '+texts[3])
        else:
            # delete from table perawat/dokter where username = texts[2]
            cursor.execute(f"DELETE FROM {texts[1]} WHERE username = '{texts[3]}' and poli = '{texts[2]}'")
            conn.commit()
            context.bot.send_message(chat_id=update.effective_chat.id, text='Berhasil menghapus '+texts[1]+' ke '+poli[int(texts[2])]+' dengan user id '+texts[3])
        
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text='Perintah tidak ditemukan. \n format perintah: \n /admin {add|remove} {perawat|dokter} {idpoli:1|2|3|4|5} {userId} \n contoh: /admin add perawat 1 456789')



def main():
    # Read the Telegram Bot API key from the environment variable
    telegram_api_key = os.getenv("TELEGRAM_BOT_API_KEY")

    # Initialize the Telegram Bot Updater
    updater = Updater(token=telegram_api_key, use_context=True) 
    bot = updater.dispatcher

    # Add handlers for Telegram Bot commands
    start_handler = CommandHandler("start", start)
    bot.add_handler(start_handler)

    info_handler = CommandHandler("info", info)
    bot.add_handler(info_handler)

    pasien_handler = CommandHandler("pasien", pasien)
    bot.add_handler(pasien_handler)

    antrian_handler = CommandHandler("antrian", antrian)
    bot.add_handler(antrian_handler)

    skip_handler = CommandHandler("skip", skip)
    bot.add_handler(skip_handler)

    ada_handler = CommandHandler("ada", ada)
    bot.add_handler(ada_handler)

    admin_handler = CommandHandler("admin", admin)
    bot.add_handler(admin_handler)

    chat_handler = MessageHandler(Filters.text, chat)
    bot.add_handler(chat_handler)

    # Start the Telegram Bot
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    print('Starting chatbot...')
    try:
        main()
    except Exception as e:
        # Log the error
        logging.error(
            "An error occurred while starting the chatbot: %s", str(e))
        print("An error occurred while starting the chatbot. Check the logs for more information.")
